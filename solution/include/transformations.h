#ifndef TRANSFORMATIONS_H
#define TRANSFORMATIONS_H

#include "image.h"

struct image* rotate(const struct image* source, int angle);

#endif // TRANSFORMATIONS_H
