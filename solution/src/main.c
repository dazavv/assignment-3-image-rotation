#include "bmp.h"
#include "transformations.h"

#include <stdio.h>
#include <stdlib.h>


FILE* open_file(const char* filename, const char* mode) {
    FILE* file = fopen(filename, mode);
    if (!file) perror("Error opening the file");
    return file;
}

//проверка корректности угла поворота
int is_valid_angle(int angle) {
    return (angle == 0 || angle == 90 || angle == -90 ||
            angle == 180 || angle == -180 || angle == 270 || angle == -270);
}

void rotate_image_and_write(const char* source_image, const char* transformed_image, int angle) {
    struct image* img = NULL;

    //открытие изображения
    FILE* open_file_img = open_file(source_image, "rb");

    if (!is_valid_angle(angle)) {
        perror("Wrong angle ");
        fclose(open_file_img);
        exit(EXIT_FAILURE);
    }

    //считываем изображение
    enum read_status read_img = from_bmp(open_file_img, &img);
    fclose(open_file_img);
    if (read_img != READ_OK) perror("Error reading the image");

    //поворот изображения на "angle" градусов
    struct image* rotated_img = rotate(img, angle);
    if (!rotated_img) perror("Error making the image");

    //файл для записи повернутого изображения
    FILE* file_rot_img = open_file(transformed_image, "wb");

    // Повернутое изображение -> файл для него
    enum write_status write_img = to_bmp(file_rot_img, rotated_img);
    fclose(file_rot_img);
    if (write_img != WRITE_OK) perror("Error writing the image");

    // Освобождение памяти
    destroy_image(img);
    destroy_image(rotated_img);
}

int main(int argc, char* argv[]) {
    if (argc != 4) {
        perror("Invalid number of arguments");
        exit(EXIT_FAILURE);
    }

    char* source_image = argv[1];//исходное изображение
    char* transformed_image = argv[2];//имя файла для сохранения повернутого изображения
    int angle = atoi(argv[3]);//градус поворота изображения (0, 90, -90, 180, -180, 270, -270)

    rotate_image_and_write(source_image, transformed_image, angle);
    return EXIT_SUCCESS;
}

