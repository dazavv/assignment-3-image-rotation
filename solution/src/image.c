#include "image.h"
#include <stdlib.h>

struct image* create_image(uint64_t width, uint64_t height) {
    struct image* img = (struct image*)malloc(sizeof(struct image));
    if (img != NULL) {
        img->data = (struct pixel*)malloc(width * height * sizeof(struct pixel));
        if (img->data == NULL) {
            free(img);
            return NULL;
        }
        img->width = width;
        img->height = height;
    }
    return img;

}

void destroy_image(struct image* img) {
    free(img ? img->data : NULL);
    free(img);
}
