#include "bmp.h"

#define BF_TYPE_DEFAULT 0x4D42
#define BF_RESERVED_DEFAULT 0
#define BI_COMPRESSION_DEFAULT 0
#define BI_CLR_USED_DEFAULT 0
#define BI_CLR_IMPORTANT_DEFAULT 0
#define BI_BIT_COUNT 24
#define BI_SIZE 40
#define BI_PLANES 1
#define BI_SIZE_IMAGE 0
#define BI_XPELS_PER_METER 0
#define BI_YPELS_PER_METER 0
#define BYTES_PER_PIXEL sizeof(struct pixel)
#define FILE_PADDING_CONSTANT 4

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

enum write_status write_bmp_header(FILE* out, const struct bmp_header* header) {
    return fwrite(header, sizeof(struct bmp_header), 1, out) == 1 ? WRITE_OK : WRITE_ERROR;
}

enum read_status read_bmp_header(FILE* in, struct bmp_header* header) {
    return fread(header, sizeof(struct bmp_header), 1, in) == 1 ? READ_OK : READ_INVALID_HEADER;
}

enum write_status write_bmp_pixels(FILE* out, const struct image* img, uint32_t padding) {
    for (uint32_t y = 0; y < img->height; ++y) {
        if (fwrite(&img->data[y * img->width], BYTES_PER_PIXEL, img->width, out) != img->width) {
            return WRITE_ERROR;
        }

        if (padding > 0 && fseek(out, padding, SEEK_CUR) != 0) {
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}


enum write_status to_bmp(FILE* out, const struct image* img) {
    struct bmp_header header;

    uint32_t size = img->width * BYTES_PER_PIXEL;
    uint32_t padding = (FILE_PADDING_CONSTANT - (size % FILE_PADDING_CONSTANT)) % FILE_PADDING_CONSTANT;


    header.bfType = BF_TYPE_DEFAULT;
    header.bfileSize = sizeof(struct bmp_header) + (size + padding) * img->height;
    header.bfReserved = BF_RESERVED_DEFAULT;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = BI_SIZE;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = BI_PLANES;
    header.biBitCount = BI_BIT_COUNT;
    header.biCompression = BI_COMPRESSION_DEFAULT;
    header.biSizeImage = BI_SIZE_IMAGE;
    header.biXPelsPerMeter = BI_XPELS_PER_METER;
    header.biYPelsPerMeter = BI_YPELS_PER_METER;
    header.biClrUsed = BI_CLR_USED_DEFAULT;
    header.biClrImportant = BI_CLR_IMPORTANT_DEFAULT;

    return write_bmp_header(out, &header) == WRITE_OK && write_bmp_pixels(out, img, padding) == WRITE_OK ? WRITE_OK : WRITE_ERROR;
}


enum read_status from_bmp(FILE* in, struct image** img) {
    struct bmp_header header;

    if (read_bmp_header(in, &header) != READ_OK || header.bfType != BF_TYPE_DEFAULT) {
        return READ_INVALID_SIGNATURE;
    }

    *img = create_image(header.biWidth, header.biHeight);
    if (!*img) {
        return READ_INVALID_BITS;
    }

    uint32_t padding = (FILE_PADDING_CONSTANT - ((*img)->width * BYTES_PER_PIXEL % FILE_PADDING_CONSTANT)) % FILE_PADDING_CONSTANT;
    for (uint32_t y = 0; y < header.biHeight; ++y) {
        if (fread(&(*img)->data[y * header.biWidth], BYTES_PER_PIXEL, header.biWidth, in) != header.biWidth) {
            return READ_INVALID_BITS;
        }
        if (fseek(in, (long)padding, SEEK_CUR) != 0) {
            return READ_INVALID_BITS;
        }
    }
    return READ_OK;
}

