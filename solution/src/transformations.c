#include "transformations.h"

#include <stddef.h>

// Определение макроса для повторяющегося кода
#define TRANSFORM(expression) \
    for (uint64_t y = 0; y < orig_height; ++y) { \
        for (uint64_t x = 0; x < orig_width; ++x) { \
            expression; \
        } \
    }

struct image* rotate(const struct image* source, int angle) {

    uint64_t orig_width = source->width;
    uint64_t orig_height = source->height;

    uint64_t rotated_width, rotated_height;

    struct image* rotated = NULL;


    if (angle == 0 || angle == 180 || angle == -180) {
        rotated_width = orig_width;
        rotated_height = orig_height;
    } else {
        rotated_width = orig_height;
        rotated_height = orig_width;
    }

    rotated = create_image(rotated_width, rotated_height);
    if (!rotated) return NULL;

    if (angle == 180 || angle == -180) {
        TRANSFORM(rotated->data[(orig_height - 1 - y) * orig_width + (orig_width - 1 - x)] = source->data[y * orig_width + x]);
    } else if (angle == 0) {
       TRANSFORM(rotated->data[y * orig_width + x] = source->data[y * orig_width + x]);
    } else if (angle == 90 || angle == -270) {
        TRANSFORM(rotated->data[(rotated_height - 1 - x) * rotated_width + y] = source->data[y * orig_width + x]);
    } else if (angle == -90 || angle == 270) {
        TRANSFORM(rotated->data[x * rotated_width + rotated_width - 1 - y] = source->data[y * orig_width + x]);
    }

    return rotated;
}
